<?php
/*
Plugin Name: Candidater
Description: Candidater pour une alternance
Author: JB
*/

// Fonction pour afficher le formulaire
function candidater()
{
    traiter_formulaire_candidature();

    ob_start();
?>
    <style>
        label {
            font-weight: bold;
        }
        input[type="text"],
        input[type="number"],
        textarea {
            width: 100%;
            padding: 5px;
            margin-bottom: 10px;
        }
    </style>

    <form action="#" method="post">
        <label for="prenom">Prénom:</label>
        <input type="text" id="prenom" name="prenom" required><br><br>

        <label for="nom">Nom:</label>
        <input type="text" id="nom" name="nom" required><br><br>

        <label for="date_naissance">Date de Naissance:</label>
        <input type="text" id="date_naissance" name="date_naissance" required><br><br>

        <label for="diplome_actuel">Libellé du Diplôme Actuel:</label>
        <input type="text" id="diplome_actuel" name="diplome_actuel" required><br><br>

        <label for="niveau_etudes">Niveau d'Études Actuel:</label>
        <input type="text" id="niveau_etudes" name="niveau_etudes" required><br><br>

        <label for="formation_visee">Formation Visée:</label>
        <input type="text" id="formation_visee" name="formation_visee" required><br><br>

        <label for="ville_habitation">Ville d'Habitation:</label>
        <input type="text" id="ville_habitation" name="ville_habitation" required><br><br>

        <label for="mobilite">Mobilité (en kilomètres depuis le lieu de vie):</label>
        <input type="number" id="mobilite" name="mobilite" required><br><br>

        <label for="texte_libre">Texte Libre:</label>
        <textarea id="texte_libre" name="texte_libre" required></textarea><br><br>

        <input type="submit" name="submit" value="Soumettre"> <!-- Ajout d'un bouton de soumission -->
    </form>
    <?php
    return ob_get_clean();
}

add_shortcode('candidater_formulaire', 'candidater');

// Fonction pour traiter le formulaire
function traiter_formulaire_candidature()
{
    global $wpdb;

    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
        $prenom = $_POST['prenom'];
        $nom = $_POST['nom'];
        $date_naissance = $_POST['date_naissance'];
        $diplome_actuel = $_POST['diplome_actuel'];
        $niveau_etudes = $_POST['niveau_etudes'];
        $formation_visee = $_POST['formation_visee'];
        $ville_habitation = $_POST['ville_habitation'];
        $mobilite = $_POST['mobilite'];
        $texte_libre = $_POST['texte_libre'];

        $table_name = $wpdb->prefix . 'registration';

        $wpdb->insert(
            $table_name,
            array(
                'prenom' => $prenom,
                'nom' => $nom,
                'date_naissance' => $date_naissance,
                'diplome_actuel' => $diplome_actuel,
                'niveau_etudes' => $niveau_etudes,
                'formation_visee' => $formation_visee,
                'ville_habitation' => $ville_habitation,
                'mobilite' => $mobilite,
                'texte_libre' => $texte_libre
            )
        );
    }
}



function afficher_contenu_base_de_donnees()
{

    ob_start();

    global $wpdb;

    $table_name = $wpdb->prefix . 'registration';

    $table_content = $wpdb->get_results(
        "SELECT * FROM $table_name"
    );

    if ($table_content) {
    ?>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th,
            td {
                border-bottom: 1px solid black;
                padding: 8px;
                text-align: center;
            }
        </style>
        <table>
            <thead>
                <tr>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Date de Naissance</th>
                    <th>Diplome actuel</th>
                    <th>Niveau études</th>
                    <th>Formation visée</th>
                    <th>Ville de résidence</th>
                    <th>Mobilité</th>
                    <th>Texte Libre</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($table_content as $row) { ?>
                    <tr>
                        <td><?php echo $row->prenom; ?></td>
                        <td><?php echo $row->nom; ?></td>
                        <td><?php echo $row->date_naissance; ?></td>
                        <td><?php echo $row->diplome_actuel; ?></td>
                        <td><?php echo $row->niveau_etudes; ?></td>
                        <td><?php echo $row->formation_visee; ?></td>
                        <td><?php echo $row->ville_habitation; ?></td>
                        <td><?php echo $row->mobilite; ?></td>
                        <td><?php echo $row->texte_libre; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
<?php
    } else {
        echo '<h2>Aucune donnée trouvée</h2>';
    }

    return ob_get_clean();
}

add_shortcode('contenu_bdd', 'afficher_contenu_base_de_donnees');





// Création de la table lors de l'activation du plugin

// register_activation_hook(__FILE__, 'alternance_create_database');

// function alternance_create_database() {
//     global $wpdb;

//     $table_name = $wpdb->prefix . 'registration';

//     $charset_collate = $wpdb->get_charset_collate();

//     $sql = "CREATE TABLE $table_name (
//         id mediumint(9) NOT NULL AUTO_INCREMENT,
//         time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
//         prenom tinytext NOT NULL,
//         nom tinytext NOT NULL,
//         date_naissance date NOT NULL,
//         diplome_actuel tinytext NOT NULL,
//         niveau_etudes tinytext NOT NULL,
//         formation_visee text NOT NULL,
//         ville_habitation tinytext NOT NULL,
//         mobilite int NOT NULL,
//         texte_libre text,
//         PRIMARY KEY  (id)
//     ) $charset_collate;";

//     require_once ABSPATH . 'wp-admin/includes/upgrade.php';
//     dbDelta($sql);
// }